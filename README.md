# [clang-format-container](https://gitlab.com/eidoom/clang-format-container)

Image with `clang-format` installed on Debian `testing`.

## Get

Container image on [Docker Hub](https://hub.docker.com/r/eidoom/clang-format).

Get it with
```shell
podman pull docker.io/library/eidoom/clang-format
```

## Use

Interactive:
```shell
podman run -it --rm eidoom/clang-format bash
```

## Build

Build with
```shell
podman build -t eidoom/clang-format .
```
then push to Docker Hub with
```shell
podman push localhost/eidoom/clang-format docker.io/eidoom/clang-format
```
